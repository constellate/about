---
title: "Goals"
date: 2017-11-15T18:38:20+01:00
---

These are the goals of our project. We must try to follow them as strictly as
possible and abide by them, in the order they are presented.

# Respect user privacy

> No one shall be subjected to arbitrary interference with his privacy, family,
> home or correspondence, nor to attacks upon his honour and reputation.
> Everyone has the right to the protection of the law against such interference
> or attacks.
>
> _-- Universal declaration of Human Rights, Article 12_

We believe user privacy to be of utmost importance. In Facebook, Instagram,
Twitter, Google+, YouTube, you name it: you are the product. Your privacy is the
product. With Constellate, together with many other projects in the Federation,
you are the consumer, as it should be.

Our software should not hold code to track the user without their consent. Our
project should not encourage the use of anything else that might harm the user
or their privacy. Our software should not provide user information to third
parties, unless, of course, this is through information obtainable via
ActivityPub.

Our software should remove any information that may involountarily expose user
information. (i.e. geo tags on photos).

# Value instance resources

We succeed if the server software can have 50 online users at the same time
while using at most 128 MB of RAM. We fail if it can't.

We should also try to minimize disk usage whenever possible. This, of course,
mostly refers to compressing media whenever possible; admins should be given the
option to not compress anything or compress everything (tradeoff here is of
course with the CPU usage, especially for video).

## Speed

We also care deeply about performance. With optimal hardware, Constellate should
run at a reasonable speed, and most API requests should be able to complete
within 100ms.

# Be beginner-friendly

Constellate should not be for any niche group. My dad should be able to get an
instance list, pick an instance with ease, get signed up and have a rough idea
of how the whole thing works. Simplicity is key, and we should carefully pick
the features we want to implement for user-facing functionality.

Reverting functionality that was in an actual point release is catastrophic. We
should try to never take away any functionality from the users.

> One might think that Mastodon's UI is easy to use and beginner friendly. It is
> not. I'm pretty sure that if I showed it to my dad, he would perhaps get the
> gist of it only after a few hours of tinkering with it. We should not aim for
> a Tweetdeck-like UI: that is for power users. Our interface should get
> inspiration from existing social networks and software that are for your
> common everyday layman.

Ideally we would also have proper designers one day, but those don't come cheap
and if a designer is interested in having a stab at contributing to FOSS, they
should definitely reach out.

# Be admin-friendly

_My dad should also be able to set up his own instance._

I mean, theorically, he could also set up his own Mastodon instance. But that
already assumes he has some general sysadmin/programming notions. We only assume
that our admins want to have their instance, and they want to put it on any
server they want.

Thanks to Go's cross-platform compatibility and its static binaries, this is
achievable. All that we will ask our sysadmin-illiterates is to run one, at most
two commands that will then try to automate everything for them, having sane and
secure defaults, and most importantly they should be able to run these on just
about any Linux distribution. Inspiration should be taken from netdata's
[kickstart.sh](https://github.com/firehol/netdata/blob/master/kickstart.sh).

For more sysadmin-literate users, Constellate should be transparent about what
it's doing, and provide clear installation instructions that are not just a bash
script to run in a terminal that will do all the magic. Sysadmins should also
be given the option to install from source.

# Maintainable, well-documented code

Finally, we should at least make an effort to write code that the future us will
be proud of. Of course, this is never the case, because the future version of
you will think that you are stupid and that he is not anymore, however we should
at least make an effort to write clear, understandable, and well-documented
code, with tests whenever necessary and continuous integration running on every
build.

Furthermore, to allow new developers to dive into the code, we should
occasionally create/update a reference guide about the architecture of our code,
where they should expect things to be and how to write code in style with ours.
