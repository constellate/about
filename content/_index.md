## Instance hosting, _back in your hands_

Constellate is a WIP project of creating an ActivityPub compliant instance
server. If you did not understand a word of what I just said, it's fine.

Constellate builds upon [Mastodon](https://joinmastodon.org), and aims to
resolve these fundamental issues:

* **Beginner-friendliness:** Mastodon is light years ahead of GNUSocial in terms
  of user friendliness, but still doesn't come anywhere close to classic social
  networks in terms of usability. Constellate aims to make it easy for users to
  switch from Twitter to a Constellate instance.
* **Admin-friendliness:** installing constellate should be a cakewalk, take only
  a few minutes, and the administrator could possibly forget to even be running
  an instance. Admin interaction should only be required when absolutely
  necessary. This means automatic updates (when user has installed through
  pre-compiled binaries).
* **Resource usage:** Mastodon is a RoR app. Which in itself brings up the
  problem of scalability. Your instance grows bigger, you keep expanding your
  server, your bill keeps growing, and now you're paying about 80€/mo for it.
  Our goal is to allow even a 3€/mo Scaleway VPS to host an instance holding
  hundreds of users and thousands of statuses.
  We deeply value especially your RAM.

At the moment, nothing really works. We're slowly building the groundwork, and
a proof-of-concept should be on its way some day.
