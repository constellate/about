function hasBeenPassed(el) {
    return pageYOffset >= el.offsetTop-5;
}

function tocOpacity() {
    var toc = [].slice.call(document.querySelectorAll("#TableOfContents a"), 0).reverse();
    if (!toc) return;
    window.onscroll = function() {
        var done = false;
        toc.forEach(function(el) {
            var targetElement = document.getElementById(el.getAttribute("href").substr(1));
            var hbp = hasBeenPassed(targetElement);
            el.setAttribute("class", !done && hbp ? "selected" : "");
            done = done ? done : hbp;
        });
    }
}

tocOpacity();
